const fs = require('fs');

const Generals = require('./generals.js');
const fileAlreadyExists = Generals.fileAlreadyExists;

exports.actionList = function f_actionList(message) {
  let content = message.content.toLowerCase();
  let id_guild = message.guild.id.toString();
  let id_author = message.author.id.toString();
  let keyword = content.substring(content.indexOf(' ')+1); // eventual keyword passed to the ?hl command

  if (!fileAlreadyExists(id_guild, 'json')) { // if the file for this guild doesn't exist yet, create it
    fs.writeFileSync(`./${id_guild}.json`, JSON.stringify({}));
  }

  if (fileAlreadyExists(id_guild, 'json')) { // if the file for this guild exists
    let hl_list = JSON.parse(fs.readFileSync(`./${id_guild}.json`));
    let author_guild_member = message.member;
    let dm_message = 'Error';

    if (!hl_list[id_author]) hl_list[id_author] = [];

    if (keyword.startsWith('reset')) {
      if (hl_list[id_author].length > 0) {
        hl_list[id_author] = [];
        dm_message = `🎉 **Remise à zéro :** votre liste de HL a été vidée sur le serveur \`${message.guild.name}\``;
      }
      else {
        dm_message = `😕 **Remise à zéro :** votre liste de HL est déjà vide sur le serveur \`${message.guild.name}\` …`;
      }
    }
    else if (hl_list[id_author].length > 0) { // if a list of hl exists for this user on this guild
      let hl_list_to_string = hl_list[id_author].join("`, `");
      let verb = hl_list[id_author].length == 1 ? 'est ton' : 'sont tes';
      dm_message = `📄 **Liste de HL :** \`${hl_list_to_string}\` ${verb} HL sur le serveur \`${message.guild.name}\``;
    }
    else { // if the author doesn't have an array of hl
      dm_message = `📄 **Liste de HL :** aucun HL sur le serveur \`${message.guild.name}\``;
    }

    // send the PM to the author
    author_guild_member.createDM().then(function (channel) {
      channel.send(dm_message).catch(console.error);
    }).catch(console.error);

    // save the modifications into the json file
    fs.writeFileSync(`./${id_guild}.json`, JSON.stringify(hl_list));
  }
}
