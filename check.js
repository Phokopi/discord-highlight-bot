const fs = require('fs');

const Generals = require('./generals.js');
const fileAlreadyExists = Generals.fileAlreadyExists;
const FULLWORD_HIGHLIGHT_SIZE = Generals.FULLWORD_HIGHLIGHT_SIZE;
const SELF_HL = Generals.SELF_HL;

const stringContains = Generals.stringContains;
const toDateString = Generals.toDateString;
const toTimeString = Generals.toTimeString;

exports.actionCheck = async function f_actionCheck(message) {
  let message_content = message.content.toLowerCase();
  let id_guild = message.guild.id.toString();
  let id_author = message.author.id.toString();
  let author_guild_member = message.member;
  if (!author_guild_member) return; // id the message's author isn't on the server anymore

  //let emoji_strings = message.guild.emojis.map(function(x) { return x.toString() });
  let links = message_content.match(/\b(http|https)?:\/\/\S+/gi);
  let skipped_content = [].concat(links); // parts of the message that should be skipped (emojis, ...)
  skipped_content = skipped_content.filter(function(val) { return val !== null; });

  if (fileAlreadyExists(id_guild, 'json')) { // if the file for this guild exists
    let data = fs.readFileSync(`./${id_guild}.json`);
    if(!data) { console.log("No data found."); return; }

      let hl_lists = JSON.parse(data);
      let user_ids = Object.keys(hl_lists);

      let curr_user = null;

      // check for each user if at least one hl word is in the message's content
      for (let i = 0; i < user_ids.length; i++) {
        try {
          curr_user = await message.guild.members.fetch(user_ids[i]);
        } catch(err) {
          continue; // user not found, continue loop for next user
        }
        if (!curr_user) {continue; console.log("PHOKO: user plus sur serveur");} // if the user isn't on the server anymore
        // code below will run only if try worked, i.e. if the user is still on the server
        let irc_nicks = ["<"+curr_user.displayName.toLowerCase()+">", "<"+curr_user.user.username.toLowerCase()+">"];
        let matrix_nicks = ["<"+curr_user.displayName.toLowerCase()+"[m]>", "<"+curr_user.user.username.toLowerCase()+"[m]>"];

        let curr_hl_list = hl_lists[curr_user.id.toString()];
        let highlighted_words = [];

        if (id_author === curr_user.id.toString() && !SELF_HL) continue; // don't self-hl if disabled
        if (!message.channel.members.has(curr_user.id.toString())) continue; // don't hl for non accessible channels

        for (let j = 0; j < curr_hl_list.length; j++) { // go through this user's highlights
          let highlight_word = curr_hl_list[j].toLowerCase();
          let full_match = highlight_word.length < FULLWORD_HIGHLIGHT_SIZE;
          let match_status = stringContains(highlight_word, message_content, fullword=full_match, skipped=skipped_content, forbidden=irc_nicks.concat(matrix_nicks));

          if (match_status === 'forbidden') break; // no HL for forbidden words (IRC <pseudo>, ...)
          if (message.mentions.users.has(curr_user.id)) break; // no HL if mentionned on Discord
          if (match_status === true) highlighted_words.push(highlight_word); // if the HL is in the content
        }

        if (highlighted_words.length === 0) continue; // no words were highlighted for this user in that message

        // send the PM to the author
        curr_user.createDM().then(function (channel) {
          let pseudo = author_guild_member.displayName;
          let mention = "<@"+author_guild_member.user.id+">";
          let channel_msg = "<#"+message.channel.id+">";
          let server_msg = message.guild.name;

          let time_creation = 'le '+toDateString(message.createdAt)+' à '+toTimeString(message.createdAt);

          let final_message = "";
          let plural_les_mots = highlighted_words.length == 1 ? "le mot" : "les mots";

          let message_highlighted = message.content;
          /*
          let regex_highlight = new RegExp();

          highlighted_words.forEach(function(highlighted_word) {
            let full_match = highlighted_word.length < FULLWORD_HIGHLIGHT_SIZE;
            if (full_match) { // full word only HL
              regex_highlight = RegExp("\\b(("+skipped_content.join("|")+")|("+highlighted_word+"))\\b", "gi");
            }
            else { // HL even if substring
              regex_highlight = RegExp("(("+skipped_content.join("|")+")|("+highlighted_word+"))", "gi");
            }
            message_highlighted = message_highlighted.replace(regex_highlight, "$1");
          });
          */

          final_message += "Highlight par **"+pseudo+"** ("+mention+") sur "+plural_les_mots+" **"+highlighted_words.join(", ")+"**.\n";
          final_message += "Channel "+channel_msg+" du serveur **"+server_msg+"** ("+time_creation+").\n";
          if (message_highlighted.length > 150) {
            final_message += "Aperçu du message : \n> "+message_highlighted.slice(0, 150)+" […]\n";
          } else {
            final_message += "Message : \n> "+message_highlighted+"\n";
          }
          final_message += "\n>> <"+message.url+"> <<";

          channel.send(final_message).catch(console.error);
        }).catch(console.error); // end createDM
      } // end for each user
  } // end if file already exists
}; // end export
