const fs = require('fs');

const Generals = require('./generals.js');
const fileAlreadyExists = Generals.fileAlreadyExists;

exports.actionAdd = function f_actionAdd(message) {
  let content = message.content.toLowerCase();
  let id_guild = message.guild.id.toString();
  let id_author = message.author.id.toString();
  let hl = content.substring(content.indexOf(' ')+1); // retrieve the word or sentence to hl

  if (!fileAlreadyExists(id_guild, 'json')) { // if the file for this guild doesn't exist yet, create it
    fs.writeFileSync(`./${id_guild}.json`, JSON.stringify({}));
  }

  if (fileAlreadyExists(id_guild, 'json')) { // if the file for this guild exists
    let hl_list = JSON.parse(fs.readFileSync(`./${id_guild}.json`));
    let author_guild_member = message.member;
    let dm_message = 'Error';

    //let emoji_strings = message.guild.emojis.map(function(x) { return x.toString() });
    //let hl_is_custom_emoji = emoji_strings.includes(hl); // don't add server emojis to HLs

    //let skip_hl = hl_is_custom_emoji; // variable to see if this HL has to be skipped

    if (!hl_list[id_author]) hl_list[id_author] = [];

    // update the user's array on this guild
    if (!hl_list[id_author].includes(hl)) { // if this hl isn't yet added for this user on this guild
      hl_list[id_author].push(hl);
      dm_message = `✅ **HL ajouté :** \`${hl}\`, sur le serveur \`${message.guild.name}\``;
    }
    //else if (hl_is_custom_emoji) {
      //dm_message = `⚠️ **HL non ajouté :** tu ne peux pas ajouter les emojis d'un serveurs à tes HLs`;
    //}
    else { // hl already exists
      dm_message = `⚠️ **HL non ajouté :** tu as déjà le HL \`${hl}\` sur le serveur \`${message.guild.name}\``;
    }

    // send the PM to the author
    author_guild_member.createDM().then(function (channel) {
      channel.send(dm_message).catch(console.error);
    }).catch(console.error);

    // save the modifications into the json file
    fs.writeFileSync(`./${id_guild}.json`, JSON.stringify(hl_list));
  }
}
