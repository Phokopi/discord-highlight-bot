const fs = require('fs');

Array.prototype.diff = function(a) {
    return this.filter(function(i) { return a.indexOf(i) < 0; });
};

// Function who tests if a file exists in the main folder
exports.fileAlreadyExists = function f_fileAlreadyExists(name, extension){
    return fs.existsSync('./' + name + '.' + extension);
};

// Function which tests whether a string is in another string or not
exports.stringContains = function f_stringContains(substring, string, fullword=False, skipped=null, forbidden=null) {
    if (fullword) {
        var res = string.split(" ");
        if (skipped) res = res.diff(skipped);
        if (forbidden && res.includes(forbidden)) return 'forbidden';
        return res.includes(substring);
    }
    else {
        var res = string;
        if (skipped) {
            skipped.forEach(function(skipped_word) {
                res = res.replace(skipped_word, "");
            });
        }
        if (forbidden) {
            for (var i = 0; i < forbidden.length; i++) {
                if (res.includes(forbidden[i])) return 'forbidden';
            }
        }
        return res.includes(substring);
    }
};

exports.toDateString = function f_toDateString(date) {
    let day = (date.getDate() <= 9 ? '0' : '')+date.getDate();
    let month = (date.getMonth()+1 <= 9 ? '0' : '')+(date.getMonth()+1);
    let year = (date.getFullYear() <= 9 ? '0' : '')+date.getFullYear();
    return day + "/" + month + "/" + year;
};

exports.toTimeString = function f_toTimeString(date) {
    let hours = (date.getHours() <= 9 ? '0' : '')+date.getHours();
    let minutes = (date.getMinutes() <= 9 ? '0' : '')+date.getMinutes();
    let seconds = (date.getSeconds() <= 9 ? '0' : '')+date.getSeconds();
    return hours + ":" + minutes + ":" + seconds;
};

exports.FULLWORD_HIGHLIGHT_SIZE = 5;
exports.SELF_HL = false;
