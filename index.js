const Check = require('./check.js');
const Add = require('./add.js');
const Remove = require('./remove.js');
const List = require('./list.js');
const Generals = require('./generals.js');

const fileAlreadyExists = Generals.fileAlreadyExists;

const Discord = require('discord.js');
const bot = new Discord.Client();

const Token = require('./token.js'); // store Discord Bot token in token.js : module.exports.TOKEN = '<token>';
bot.login(Token.TOKEN)
  .then(() => console.log('Logged in'))
  .catch(console.error);

bot.on('ready', function() {
  process.argv.forEach(function (val, index, array) {
    if (index >= 2 && val === '--refresh-avatar') {
      bot.user.setAvatar('./DiscordHL.png')
        .then(() => console.log('Refreshed the avatar'))
        .catch(console.error);
    }
  });

  bot.user.setActivity('&help', { type: 'PLAYING' })
    .then(() => console.log('Game changed'))
    .catch(console.error);
});

const HELP_MESSAGE =
"Ce bot de highlight fonctionne par serveur : **sur chaque serveur, il te faudra établir ta liste de mots à HL**.\n"+
"Pour chaque message contenant un de tes HL, je t'enverrai un **message privé** pour t'en informer !\n\n"+
"`&help` : afficher l'aide, en message privé\n"+
"`?hl reset` : **vider** ta liste de HL sur un serveur\n\n"+
"`+hl <mot/phrase>` : **ajouter** un mot ou une phrase à ta liste de HL sur un serveur\n"+
"`-hl <mot/phrase>` : **enlever** un mot ou une phrase de ta liste de HL sur un serveur\n"+
"`?hl` : **lister** tes HL sur un serveur\n";

// actions that will be done on every new message read
bot.on('message', function(message) {
  // if the message was sent in a guild and was not sent by a bot (i.e. was sent by a human account)
  if (message.guild && !message.author.bot) { // guild-related commands, meaning the result depends on the guild the message was sent in
    if (message.content.toLowerCase().startsWith('&help')) { // help command
      message.deletable ? message.delete() : console.log('Could not delete a message ('+message.content+') ... Missing permission ?');
      message.member.createDM().then(function (channel) {
        channel.send(HELP_MESSAGE).catch(console.error);
      }).catch(console.error);
    }
    else if (message.content.toLowerCase().startsWith('?hl')) { // list command
      message.deletable ? message.delete() : console.log('Could not delete a message ('+message.content+') ... Missing permission ?');
      List.actionList(message);
    }
    else if (message.content.toLowerCase().startsWith('+hl')) { // if the author wants to add a hl to their list
      message.deletable ? message.delete() : console.log('Could not delete a message ('+message.content+') ... Missing permission ?');
      if (message.content.split(' ').length <= 1) return;
      Add.actionAdd(message);
    }
    else if (message.content.toLowerCase().startsWith('-hl')) { // if the author wants to remove a hl from their list
      message.deletable ? message.delete() : console.log('Could not delete a message ('+message.content+') ... Missing permission ?');
      if (message.content.split(' ').length <= 1) return;
      Remove.actionRemove(message);
    }
    else { // else we just check if the message contains a HL for every member of the guild
      Check.actionCheck(message);
    }
  }
  // if the message was sent directly to the bot in a DM
  else {
    if (message.content.toLowerCase().startsWith('&help')) {
      message.reply(HELP_MESSAGE).catch(console.error);
    }
  }
});
