# Discord Highlight Bot

## What is it all about ?

Each user can have a list of highlighted keywords on each Discord server where the bot is present.

It means that a user will have a different list of highlights on a server A and on a server B, and that adding or removing highlights is based on the server where the command is sent.

## Starting the bot

**Don't forget to install `discord.js` via `npm install discord.js --save`.**

First, provide your bot's token in a `token.js` file (`module.exports.TOKEN = '<your bot token>';`).

Just type `node index.js` to run the bot. *It's also possible to refresh the bot's avatar by typing `node index.js --refresh-avatar`.*

## List of available commands

+ `&help` : send a DM with the list of available commands
+ `+hl <word/sentence>` : add an HL on this server
+ `-hl <word/sentence>` : remove an HL on this server
+ `?hl` : list HLs on this server
+ `?hl reset` : reset HLs on this server
